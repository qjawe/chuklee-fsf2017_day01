
//Load our libraries
var express = require("express");
var path = require("path");

//Create an instance of express application
var app = express();

//Define routes
app.use(function(req, resp) {

    resp.status(404);
    resp.type("text/html"); //Representation
    resp.send("<h1>File not file</h1><p>The current time is " + new Date() + "</p>");

});

app.use(express.static(__dirname + "/public"));

//Setting the port as a property of the app
app.set("port", 3000);

//Start the server on the specified port
app.listen(app.get("port"), function() {
    console.log("Application is listening on port " + app.get("port"));
    console.log("Yaaaah..");
});